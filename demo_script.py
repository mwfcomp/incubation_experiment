from config_interpreter import ConfigInterpreter
import config_interpreter

# config_interpreter.detected_from_fcs = config_interpreter.pmt_scaled_detect_all_replicates
config_interpreter.get_particles_per_cell = config_interpreter.particles_per_cell_median_of_samples
cfg = ConfigInterpreter('./sample_data/leo_480nm_capsule_thp1.ini')
print(cfg.times)
print(cfg.associated_per_cell)
pass