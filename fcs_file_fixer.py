import os
from os import remove
import sys
import fcsparser
import uuid

# Apogee flow cytometers have an issue with FCS files they produce; this script fixes it.

bad_byte_str = b'/                    $P1N'
fixed_byte_str = b'/$P1N'
byte_diff = len(bad_byte_str) - len(fixed_byte_str)

def fix_fcs(file_path):
    uid = uuid.uuid1()
    mod_file_path = file_path.split(os.path.sep)[-1] + str(uid) + '_mod'
    fixed_file_name = mod_file_path
    # fixed_file_name = os.path.join(os.curdir, os.path.sep, mod_file_path)
    fix_single_file(file_path, fixed_file_name)
    return fixed_file_name


# warning: this loads all the data into memory, may not be appropriate for super big files
def fix_single_file(in_file_name, out_file_name):
    with open(in_file_name, 'rb') as file:
        data = file.read().splitlines(True)
        first_line = data[0]
        if first_line_needs_fixing(first_line):
            first_line = fix_broken_first_line(first_line)
        data[0] = first_line
    dirs, _ = os.path.split(out_file_name)
    if dirs != '':
        os.makedirs(dirs, exist_ok=True)
    with open(out_file_name, 'wb') as out_file:
        for line in data:
            out_file.write(line)


def first_line_needs_fixing(first_line):
    return bad_byte_str in first_line


def fix_broken_first_line(first_line):
    text_start = int(first_line[6:18])
    new_header = fix_broken_header(first_line[0:text_start])
    new_text = fix_broken_text(first_line[text_start:])

    new_first_line = new_header + new_text  # ' {}{}'.format(new_header, new_text)
    # new_first_line = bytes(new_first_line, encoding='ascii')
    return new_first_line


def fix_broken_header(header):
    version_str = header[0:10]
    byte_indicators_str = header[10:]
    byte_header_len = 8
    bis_split = [byte_indicators_str[i:i+8] for i in range(0, len(byte_indicators_str), byte_header_len)]
    byte_indicators = [int(b) for b in bis_split]
    # byte_indicators = [int(b) for b in byte_indicators_str.split()]
    text_start = int(byte_indicators[0])

    new_byte_indicators = [b - byte_diff if b > text_start else b
                           for b in byte_indicators]

    # new_header = '{}    '.format(version_str)
    new_header = version_str
    for byte_indicator in new_byte_indicators:
        byte_indicator_str = '{:>8}'.format(byte_indicator)
        byte_indicator_bytes = bytes(byte_indicator_str, encoding='ascii')
        new_header = new_header + byte_indicator_bytes
        # new_header = new_header + '{:>8}'.format(byte_indicator)
    return new_header


def fix_broken_text(text):
    return text.replace(bad_byte_str, fixed_byte_str)


def convert_and_process_sample(file_path):
    uid = uuid.uuid1()
    mod_file_path = file_path.split(os.path.sep)[-1] + str(uid) + '_mod'
    fixed_file_name = mod_file_path
    # fixed_file_name = os.path.join(os.curdir, os.path.sep, mod_file_path)
    fix_single_file(file_path, fixed_file_name)
    meta, sample = fcsparser.parse(fixed_file_name, reformat_meta=True)
    remove(fixed_file_name)
    return sample


def convert_fcs_file_to_csv(file_path):
    sample = convert_and_process_sample(file_path)
    csv_file_path = '{}.csv'.format(os.path.splitext(file_path)[0])
    sample.to_csv(csv_file_path)


if __name__ == "__main__":
    for file_path in sys.argv[1:]:
        convert_fcs_file_to_csv(file_path)
