import math
from pathlib import Path
from typing import *
from configparser import ConfigParser
from experimental_detail import *

import json
from fcs_file_fixer import convert_and_process_sample
import os.path
import pandas as pd
import numpy as np

from math import isnan
from scipy.stats import sem


def raw_detect_all_replicates_fcs(config_filepath: str, section_name: str, key_name: str) -> [pd.Series]:
    config = ConfigParser()
    config.read(config_filepath)
    section = config[section_name]
    channel_name = json.loads(section['detect_channels'])[0]
    file_names_or_values_json = json.loads(section[key_name])
    if is_file_names(file_names_or_values_json):  # the usual case
        full_file_names = [get_real_path(config_filepath, section_name, file) for file in file_names_or_values_json]
        frames = [convert_and_process_sample(file) for file in full_file_names]
        series = [frame[channel_name] for frame in frames]
    else:  # you can also just directly specify the value in the section
        values = json.loads(file_names_or_values_json)
        series = [pd.Series({channel_name: [v]}) for v in values]
    return series


def pmt_scaled_detect_all_replicates(config_filepath: str, section_name: str, key_name: str) -> [pd.Series]:
    config = ConfigParser()
    config.read(config_filepath)
    section = config[section_name]
    channel = json.loads(section['detect_channels'])[0]
    file_names_or_values_json = json.loads(section[key_name])
    particle_control_files = json.loads(section['particle_signal_controls'])
    if not 'pmt_exponential_response' in config['model'] or not is_file_names(file_names_or_values_json):
        return raw_detect_all_replicates_fcs(config_filepath, section_name, key_name)
    exp_scale = config['model'].getfloat('pmt_exponential_response')
    full_file_names = [get_real_path(config_filepath, section_name, file) for file in file_names_or_values_json]
    all_series = raw_detect_all_replicates_fcs(config_filepath, section_name, key_name)
    # ASSUMPTION: ALL PARTICLE SIGNAL CONTROLS USING SAME PMT VOLTAGE
    base_voltage = channel_pmt_voltage(channel, get_real_path(config_filepath, section_name, particle_control_files[0]))

    for idx, series in enumerate(all_series):
        scale_voltage = channel_pmt_voltage(channel, full_file_names[idx])
        scaling = math.exp(exp_scale * base_voltage) / math.exp(exp_scale * scale_voltage)
        series.loc[:] = series.loc[:] * scaling
    return all_series


def particles_per_cell_combine_samples(sample_reps: [pd.Series],
                                       particle_ctrl_reps: [pd.Series],
                                       cell_ctrl_reps: [pd.Series]):
    samples = pd.concat(sample_reps)
    # [np.median(s) for s in sample_reps]
    particle_ctrl = np.median(pd.concat(particle_ctrl_reps))
    cell_ctrl = np.median(pd.concat(cell_ctrl_reps))
    all_particles_per_cell = [(s - cell_ctrl) / particle_ctrl for s in samples]
    return np.array(all_particles_per_cell)


def particles_per_cell_median_of_samples(sample_reps: [pd.Series],
                                         particle_ctrl_reps: [pd.Series],
                                         cell_ctrl_reps: [pd.Series]):
    samples = [np.median(s) for s in sample_reps]
    particle_ctrl = np.median(pd.concat(particle_ctrl_reps))
    cell_ctrl = np.median(pd.concat(cell_ctrl_reps))
    all_particles_per_cell = [(s - cell_ctrl) / particle_ctrl for s in samples]
    return np.array(all_particles_per_cell)


combine_particles_per_cell = np.median
get_particles_per_cell = particles_per_cell_combine_samples
detected_from_fcs = pmt_scaled_detect_all_replicates


class ConfigInterpreter:
    def __init__(self, config_file: str):
        self.config_file = config_file
        self.config = ConfigParser()
        self.config.read(self.config_file)
        self.times = []
        self.all_associated_per_cell_values = []
        self._assign_times_vals()
        self.associated_per_cell = [combine_particles_per_cell(vals) for vals in self.all_associated_per_cell_values]
        self.associated_per_cell_std_dev = [np.std(vals) for vals in self.all_associated_per_cell_values]
        self.lower_quartiles = np.array([np.quantile(m, 0.25) for m in self.all_associated_per_cell_values])
        self.upper_quartiles = np.array([np.quantile(m, 0.75) for m in self.all_associated_per_cell_values])
        self.experimental_and_cell_detail = self._get_experimental_and_cell_detail()

    @staticmethod
    def times_vals_dispersions_from_pre_specified_ini(config: ConfigParser, value_name) -> (
            Iterable[int], Iterable[float], Iterable[float]):
        time_pt_sections = [s for s in config.sections() if s.startswith('time_point')]
        times = []
        all_vals = []
        for section in time_pt_sections:
            time = float(config[section]['time_in_mins'])
            vals = json.loads(config[section][value_name])
            times.append(time)
            all_vals.append(vals)
        # assuming we want dispersion to be standard error of the mean
        all_dispersions = [sem(vals) for vals in all_vals]
        all_dispersions = [0.0 if isnan(d) else d for d in all_dispersions]
        return times, all_vals, all_dispersions

    def get_dataframe(self) -> pd.DataFrame:
        # dir, file = os.path.split(self.config_file)
        # file_name = '{}_data.csv'.format(os.path.splitext(file)[0])
        # full_file_name = os.path.join(dir, file_name)
        df = pd.DataFrame({
            'config': self.config_file,
            'particle_name': self.experimental_and_cell_detail.particle.name,
            'radius_in_m': self.experimental_and_cell_detail.particle.radius,
            'density_in_kg_per_m3': self.experimental_and_cell_detail.particle.density,
            'cell_line_name': self.experimental_and_cell_detail.cell_info.cell_line,
            'time_in_mins': self.times,
            'value': self.associated_per_cell_means,
            'value_std_dev': self.associated_per_cell_means_std_dev
        })
        return df

    def to_csv(self, suffix: str):
        basefile = os.path.splitext(self.config_file)[0]
        filename = '{}_{}.csv'.format(basefile, suffix)
        df = self.get_dataframe()
        df.to_csv(filename)

    def _get_experimental_and_cell_detail(self) -> ExperimentalAndCellDetail:
        cell_info = CellInfo(cell_line=self.config['cell']['cell_line_name'],
                             total_cells=float(self.config['cell']['total_cells']),
                             surface_area_per_cell=float(self.config['cell']['surface_area_per_cell_in_m2']))

        culture = self.config['culture']
        width = float(culture['width_in_m'])
        height = float(culture['height_in_m'])
        depth = float(culture.get('depth_in_m', '0'))
        if depth == 0:
            cell_culture = CylindricalCellCultureCondition(width=width, height=height,
                                                           temperature=float(culture['temp_in_k']),
                                                           time_in_seconds=self.times[-1] * 60,
                                                           # culture condition time is in seconds, config is in minutes
                                                           particle_concentration=0,
                                                           cell_orientation=CellOrientation.bottom,
                                                           flow_speed=0)
        else:
            cell_culture = RectangularCellCultureCondition(width=width, height=height, depth=depth,
                                                           temperature=float(culture['temp_in_k']),
                                                           time_in_seconds=self.times[-1] * 60,
                                                           # culture condition time is in seconds, config is in minutes
                                                           particle_concentration=0,
                                                           cell_orientation=CellOrientation.bottom,
                                                           flow_speed=0)

        conc = float(culture['particles_per_cell']) / (width * height)
        cell_culture.particle_concentration = conc
        particle_info = self.config['particle']
        particle = SphericalParticle(name=particle_info['particle_name'], radius=float(particle_info['radius_in_m']),
                                     density=float(particle_info['density_in_kg_per_m3']))

        media = LiquidMedia(viscosity=float(culture['media_viscosity_in_kg_per_m_s']),
                            density=float(culture['media_density_in_kg_per_m3']))

        experimental_detail = ExperimentalAndCellDetail(particle=particle, media=media,
                                                        cell_culture_condition=cell_culture,
                                                        cell_info=cell_info)
        return experimental_detail

    def _assign_times_vals(self) -> None:
        particles_per_cell_specified = self.config['model'].get('particles_per_cell_determined', 'False') == 'True'

        if particles_per_cell_specified:
            self.times, self.all_associated_per_cell_values, self.all_associated_per_cell_dispersions = \
                ConfigInterpreter.times_vals_dispersions_from_pre_specified_ini(self.config, 'particles_per_cell')
        else:
            time_pt_sections = [s for s in self.config.sections() if s.startswith('time_point')]

            for section_name in time_pt_sections:
                sample_reps = detected_from_fcs(self.config_file, section_name, 'experiment_data')
                particle_ctrl_reps = detected_from_fcs(self.config_file, section_name, 'particle_signal_controls')
                cell_ctrl_reps = detected_from_fcs(self.config_file, section_name, 'background_controls')
                time_in_mins = json.loads(self.config[section_name]['time_in_mins'])
                particles_per_cell = get_particles_per_cell(sample_reps, particle_ctrl_reps, cell_ctrl_reps)
                self.times.append(time_in_mins)
                self.all_associated_per_cell_values.append(particles_per_cell)
                # self.all_associated_per_cell_dispersions.append(dispersions)


def get_real_path(config_filepath: str, section_name: str, file_name: str) -> str:
    config = ConfigParser()
    config.read(config_filepath)
    section = config[section_name]
    real_base = Path(config_filepath).parent.joinpath(Path(section['base_folder']))
    return str(real_base.joinpath(Path(file_name)))


def channel_pmt_voltage(channel: str, data_file: str):
    channel_start = channel.split('(')[0]
    params = []
    voltages = []
    with open(data_file, encoding='latin-1') as f:
        for line in f:
            if line.startswith('PARAM'):
                params = line.split(';')
            if line.startswith('PMTVOLTAGES'):
                voltages = line.split(';')
                break
    idx = params.index(channel_start)
    return float(voltages[idx])


def is_file_names(json):
    return type(json[0]) is str
